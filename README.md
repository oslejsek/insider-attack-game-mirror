# Cybersecurity game "Locust 3302" adapted to insider attack simulation

This cybersecurity game allows a **hands-on demonstration and practice** of topics such as network scanning, SSH connections, and password cracking. It is suitable for undergraduate students in computing.

Please follow the [general instructions](https://gitlab.ics.muni.cz/muni-kypo-trainings/games/all-games-index) to set up the game, learn more about its [licensing](https://gitlab.ics.muni.cz/muni-kypo-trainings/games/all-games-index#license), and see how to [cite it](https://gitlab.ics.muni.cz/muni-kypo-trainings/games/all-games-index#how-to-cite-the-games).

This game is based on original Locust 3302

## Credits (Locust 3302)

[Cybersecurity Laboratory](https://cybersec.fi.muni.cz)\
Faculty of Informatics\
Masaryk University

**Leading authors:** [Adam Chovanec](https://github.com/chovanecadam), Hana Pospíšilová, [Peter Jaško](https://github.com/jaskp)

**Contributors/Consultants:** Valdemar Švábenský, Jan Vykopal, Milan Čermák, Martin Laštovička
